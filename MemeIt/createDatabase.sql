-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2019 at 07:43 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meme` longblob,
  `visible` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `title`, `meme`, `visible`) VALUES
(1, 'Google - Web Performance Best Practices', NULL, 0),
(2, 'HTML5 - част 1 семантични тагове. Примери.', NULL, 0),
(3, 'HTML5 - част 2 форми. Примери.', NULL, 0),
(4, 'CSS: стилове, класове, селектори', NULL, 0),
(5, 'CSS: layouts, box model', NULL, 0),
(6, 'CSS: layouts, flexbox', NULL, 0),
(7, 'Анимации с CSS с използване на трансформации', NULL, 0),
(8, 'JavaScript - част 1', NULL, 0),
(9, 'DOM дърво, обхождане и манипулация', NULL, 0),
(10, 'Работа със сесии и cookies (от страна насървъра и клиента)', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fn` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `password` varchar(2048) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `fn`, `topic_id`, `password`) VALUES
(1, 'nicolesvemanuilova@gmail.com', 'Никол Емануилова', 61987, 9, '$2y$10$VHHO9dt408GAgOxCfCzSCeUIk7XgvOZl7nwvOKVevsDrWo/I07LHa'),
(2, 'silvia@gmail.com', 'Силвия Иванова', 61999, 10, '$2y$10$PRQNnmrHdyozjux/ZmgvLOHxhpsz556J/LNcmwLD/eqAqt8jiYrKK'),
(3, 'alex@gmail.com', 'Александър Димитров', 62987, 4, '$2y$10$RVfp4QQAFTHviHcLi3y5..ShxE5pMDwMVRCwvY/a1tVRYGUusQ.1W');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topic_id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_topic` (`topic_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_topic` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
